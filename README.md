# File Project CGE

crear virtual enviroment

```
$ python3 -m venv .venv 
$ source .venv/bin/activate
```
instalar dependencias
```
$ pip3 install -r requirements.txt
```
se debe configurar el archivo **.env** creando una apartir de **.env.sample**
definir las rutas que se solicitan.

* El archivo **pathdata.json** es requerido, si se quiere usar el analisis por un array de path's definido 
* Si se quiere analizar todo un directorio definir el path en la variable PATH_FILE de **.env** en este directorio solo debe existir archivos tipo **.cge**.

Ejemplo:
```
$ ls -la /ARCHIVO

total 752K
-rw-r--r-- 1 gcasas gcasas  83K may 17 20:48 4fdb-41e3-982b-7389dcc0-b8012ed78696.cge
-rw-r--r-- 1 gcasas gcasas 156K may 17 19:26 6bcf9386-37c7-4ba9-8d3d-4ef745498765.cge
-rw-r--r-- 1 gcasas gcasas 214K ene 10 09:17 7389dcc0-4fdb-41e3-982b-b8012ed78696.cge
-rw-r--r-- 1 gcasas gcasas  77K may 17 20:46 d571a834e97c-e54c506d-9853-49d2-87bc.cge
-rw-r--r-- 1 gcasas gcasas 214K ene 10 10:53 e54c506d-9853-49d2-87bc-d571a834e97c.cge
```