#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 10 23:27:08 2020

@author: gcasas
"""

import os
import json
from dotenv import load_dotenv
from fileAnalize import *
load_dotenv()

path = os.getenv("PATH_FILE")

num_pages = getNumberPages(path=path, arr_dir=None)

dataset = getFeaturesDataset(num_pages, model)
result = getEqualPDF(dataset)
f = open(os.getenv("FROM_PATH_FILE_OUTPUT"), "w")
f.write('{"data": %s}' % str(result).replace("'", '"'))
f.close()
