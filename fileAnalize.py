#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May  3 17:00:17 2020

@author: gcasas
"""

from pdf2image import convert_from_path, convert_from_bytes, pdfinfo_from_bytes
from base64 import b64decode
import matplotlib.pyplot as plt
import numpy as np
import os, io
from PyPDF2 import PdfFileReader
from keras.preprocessing import image
from keras.applications.xception import Xception
from keras import Sequential
from keras.layers import Dense, Conv2D, BatchNormalization, Activation, SeparableConv2D, MaxPooling2D, GlobalAveragePooling2D

model = Sequential()
model.add(Conv2D(32, (3, 3), strides=(2, 2), use_bias=False, name='block1_conv1'))
model.add(BatchNormalization(name='block1_conv1_bn'))
model.add(Activation('relu', name='block1_conv1_act'))
model.add(Conv2D(64, (3, 3), use_bias=False, name='block1_conv2'))
model.add(BatchNormalization(name='block1_conv2_bn'))
model.add(Activation('relu', name='block1_conv2_act'))
model.add(Conv2D(128, (1, 1), strides=(2, 2),padding='same', use_bias=False))
model.add(BatchNormalization())
model.add(SeparableConv2D(128, (3, 3), padding='same', use_bias=False, name='block2_sepconv1'))
model.add(BatchNormalization(name='block2_sepconv1_bn'))
model.add(Activation('relu', name='block2_sepconv2_act'))
model.add(SeparableConv2D(128, (3, 3), padding='same', use_bias=False, name='block2_sepconv2'))
model.add(BatchNormalization(name='block2_sepconv2_bn'))
model.add(MaxPooling2D((3, 3), strides=(2, 2), padding='same', name='block2_pool'))
model.add(Conv2D(256, (1, 1), strides=(2, 2),padding='same', use_bias=False))
model.add(BatchNormalization())
model.add(Activation('relu', name='block3_sepconv1_act'))
model.add(SeparableConv2D(256, (3, 3), padding='same', use_bias=False, name='block3_sepconv1'))
model.add(BatchNormalization(name='block3_sepconv1_bn'))
model.add(Activation('relu', name='block3_sepconv2_act'))
model.add(SeparableConv2D(256, (3, 3), padding='same', use_bias=False, name='block3_sepconv2'))
model.add(BatchNormalization(name='block3_sepconv2_bn'))
model.add(MaxPooling2D((3, 3), strides=(2, 2), padding='same', name='block3_pool'))
model.add(Conv2D(728, (1, 1), strides=(2, 2),padding='same', use_bias=False))
model.add(BatchNormalization())
model.add(Activation('relu', name='block4_sepconv1_act'))
model.add(SeparableConv2D(728, (3, 3), padding='same', use_bias=False, name='block4_sepconv1'))
model.add(BatchNormalization(name='block4_sepconv1_bn'))
model.add(Activation('relu', name='block4_sepconv2_act'))
model.add(SeparableConv2D(728, (3, 3), padding='same', use_bias=False, name='block4_sepconv2'))
model.add(BatchNormalization(name='block4_sepconv2_bn'))
model.add(MaxPooling2D((3, 3), strides=(2, 2), padding='same', name='block4_pool'))
model.add(Conv2D(1024, (1, 1), strides=(2, 2),padding='same', use_bias=False))
model.add(BatchNormalization())
model.add(GlobalAveragePooling2D())

model1 = Sequential()
model1.add(Conv2D(512, (1, 1), strides=(2, 2), padding='same'))
model1.add(Activation('relu', name='block1_conv3_acssst'))
model1.add(MaxPooling2D((1, 1), strides=(2, 2), padding='same', name='block4_poosl'))
model1.add(Activation('relu', name='block1_convs25_acst'))
model1.add(Conv2D(1024, (1, 1), strides=(2, 2), padding='same'))
model1.add(Activation('relu', name='block1_conv3_acssssssst'))
model1.add(GlobalAveragePooling2D())

def getFeatureOfPdf(path, modelFeature):
  b64 = open(path).read()
  bytes = b64decode(b64, validate=True)
  images = convert_from_bytes(bytes)
  array_images = np.empty((len(images), 512, 512, 3))
  for i in range(len(images)):
    x = images[i].resize((512, 512))
    x = image.img_to_array(x)
    x = x.reshape((1, x.shape[0], x.shape[1], x.shape[2]))
    array_images[i] = x
  features = modelFeature.predict(array_images)
  return features

def getNumberPages(path, arr_dir=None):
  if arr_dir:
    list_dir = arr_dir
  else:
    list_dir = os.listdir(path)
  num_pages = []
  for i in range(len(list_dir)):
    ruta = os.path.join(path, list_dir[i]) if not arr_dir else list_dir[i]
    b64 = open(ruta).read()
    b64 = b64decode(b64, validate=True)
    if b64[0:4] == b'%PDF':
      pdf = PdfFileReader(io.BytesIO(b64))
      num_pages.append([int(pdf.getNumPages()), ruta])
  return num_pages

def getFeaturesDataset(num_pages, model):
  # Numero de paginas por pdf
  pages_pdfs = np.array(num_pages)
  uniques = np.unique(pages_pdfs[:,0])
  array_dataset = []
  for i in uniques:
    # obtener lon indices de los pdf's que tiene una cantidad de paginas (agrupar los pdf por cantida de paginas)
    values = np.where(pages_pdfs==i)[0]
    if values.size > 1:
      # solo ingresan los pdfs de una determinada cantidad de paginas que se repitan mas de una vez(ej: 5 pdf de 1 pagina)
      array = []
      for p in values:
        arr = getFeatureOfPdf(num_pages[p][1], model)
        array.append([num_pages[p][1], arr])
      array_dataset.append(array)
  return array_dataset

def getEqualPDF(array_dataset):
  analizeRep = array_dataset
  result = []
  for idx in range(len(analizeRep)):
    for i in range(len(analizeRep[idx])):
      if not np.all((analizeRep[idx][i][1]==0)):
        print('========>', analizeRep[idx][i][0])
      r = []
      for j in range(len(analizeRep[idx])):
        if i != j:
          if np.equal(analizeRep[idx][i][1], analizeRep[idx][j][1]).all() and not np.all((analizeRep[idx][j][1]==0)):
            analizeRep[idx][j][1] = np.zeros((analizeRep[idx][j][1].shape[0], 1024))
            print(analizeRep[idx][j][0])
            r.append(analizeRep[idx][j][0])
      if len(r) >= 1:
        r.append(analizeRep[idx][i][0])
        result.append(r)
  return result

def getSimilarPDF(array_dataset):
  analizeRep = array_dataset
  result = []
  for idx in range(len(analizeRep)):
    for i in range(len(analizeRep[idx])):
      if not np.all((analizeRep[idx][i][1]==0)):
        print(analizeRep[idx][i][1].shape, '========>', analizeRep[idx][i][0])
      r = []
      for j in range(len(analizeRep[idx])):
        if i != j and not np.all((analizeRep[idx][j][1]==0)):
          total_indices = analizeRep[idx][i][1].shape[0] * analizeRep[idx][i][1].shape[1]
          equal = np.equal(analizeRep[idx][i][1], analizeRep[idx][j][1])
          true_indices = 0
          for pg in range(equal.shape[0]):
            #print(np.where(equal[pg]==True))
            true_indices = true_indices + np.where(equal[pg]==True)[0].size
          if (true_indices/total_indices) * 100 > 50.:
            r.append(analizeRep[idx][j][0])
            print((true_indices/total_indices) * 100, analizeRep[idx][j][0])
            analizeRep[idx][j][1] = np.zeros((analizeRep[idx][j][1].shape[0], 1024))
      if len(r) >= 1:
        r.append(analizeRep[idx][i][0])
        result.append(r)
  return result
